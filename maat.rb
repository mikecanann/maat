#!/usr/bin/ruby
#===============================================================================
#
#         FILE: maat.rb
#
#        USAGE: ./maat.rb
#
#  DESCRIPTION: connect to remote servers over ssh and graph system status
#
#      OPTIONS: ---
# REQUIREMENTS:
#                 yum install ruby-rrdtool
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Mike Canann (mrc), mikecanann@gmail.com
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 09/28/2013 09:57:17 AM
#     REVISION: ---
#===============================================================================


# update the system path to pull in the RRD library
BEGIN {
   if(RUBY_VERSION.include?("1.8"))
      # run with ruby 1.8
      # /usr/bin/ruby rrd.rb

      # $: << '/usr/lib/ruby/site_ruby/1.8/i386-linux'
      $LOAD_PATH.unshift('/usr/lib/ruby/site_ruby/1.8/i386-linux')
   end

   if(RUBY_VERSION.include?("1.9"))
      $LOAD_PATH.unshift('/usr/local/lib/ruby/gems/1.9.1/i386-linux')
   end
}

require 'rubygems'

require "RRD"

require 'postgres' # connect to PostGreSql

# Turn off buffering
STDIN.sync = true
STDOUT.sync = true


def create(rrd_file, data_type, consolidate_function)

   # only if the file doesn't exist
   # needs to be deleted manually if changing the data being stored
   if(!File.exists?("data"))
      Dir.mkdir("data", 0777)
   end
   if(!File.exists?("img"))
      Dir.mkdir("img", 0777)
   end
   if(!File.exist?(rrd_file))
      #DS:#{@dataset}:#{@type} ( GAUGE | COUNTER | DERIVE | ABSOLUTE COMPUTE ):#{@heartbeat}:#{@min}:#{@max}
      # RRA:AVERAGE | MIN | MAX | LAST:xff:steps:rows
      #    xff = percent made up of unknown data
      #    steps = how many primary data points are used to build a consolidated data point
      #    rows = how many generations of values are kept
      RRD.create(
         rrd_file,
         "--start", "#{Time.now.to_i - 1}", # time in seconds when the first value should be added
         "--step", "60", # update interval
         "DS:ds0:#{data_type}:120:U:U", # 120 seconds before becoming unknown
         "RRA:#{consolidate_function}:0.5:1:#{60 * 4}" # 60 * 60 * 4 = 14400 (4 hours)
         )

      puts "creating #{rrd_file}"
   end
end


def rrd_name(server_name, test)
   "./data/#{server_name}_#{test}.rrd"
end
def png_name(server_name, test)
   "./img/#{server_name}_#{test}.png"
end

def server_get_data(user, server_name, test_cmd)
   #%x(test_cmd)
   %x(ssh #{user}@#{server_name} #{test_cmd})
end

def server_save_data(cur_test, server_name, data_type, consolidate_function, actual_value, label, units_exponent)
   rrd = rrd_name(server_name, cur_test)
   png_name  = png_name(server_name, cur_test)
   create(rrd, data_type, consolidate_function) # only if it doesn't exist
   RRD.update(rrd, "#{Time.now.to_i}:#{actual_value}")
   puts "updating #{rrd} adding #{actual_value} to #{cur_test}"
   generate_png(rrd, png_name, "#{server_name} - #{cur_test}", label, consolidate_function, units_exponent)

end

def update_server_rrd(server_name)

   free_output = server_get_data("galcg3", server_name, "free -b")

   #free_output.split(" ")[7]  ==  total memory
   #free_output.split(" ")[8]  ==  used memory
   #free_output.split(" ")[9]  ==  free memory

   ###########################################################################
   # percent memory free
   percent_free = free_output.split(" ")[9].to_f / free_output.split(" ")[7].to_f
   ###########################################################################
   server_save_data("percent_free_mem", server_name, "GAUGE", "LAST", percent_free, "% free", 0)
   ###########################################################################



   ###########################################################################
   percent_swap_free = free_output.split(" ")[20].to_f / free_output.split(" ")[18].to_f
   ###########################################################################
   server_save_data("percent_swap_free", server_name, "GAUGE", "LAST", percent_swap_free, "% free", 0)
   ###########################################################################


   #mpstat -P ALL 1 3|grep -i 'Average:' | grep -e "CPU" -e "all"
   #mpstat -P ALL 1 3|grep -i 'Average:' | grep -i "all"
   #Average:     CPU   %user   %nice    %sys %iowait    %irq   %soft  %steal   %idle    intr/s
   #Average:     all    0.87    0.00    0.96    0.00    0.03    1.89    0.00   96.26   1132.00
   #mpstat_header  = %x(mpstat -P ALL 1 3|grep -i 'Average:' | grep -i CPU)
   # mrc - save this header value, only run once:
   #mpstat_header = server_get_data("galcg3", server_name, "mpstat -P ALL 1 3|grep -i 'Average:' | grep -i CPU")
   mpstat_header = server_get_data("galcg3", server_name, "mpstat -P ALL | grep -i CPU") # only one run
   #mpstat_data  = %x(mpstat -P ALL 1 3|grep -i 'Average:' | grep -i all)
   #mpstat_data = server_get_data("galcg3", server_name, "mpstat -P ALL 1 3|grep -i 'Average:' | grep -i all") # three runs
   mpstat_data = server_get_data("galcg3", server_name, "mpstat -P ALL | grep -i all") # only one run

   mpstat_header.gsub!("%user", "%usr") # change from old version to new

   percent_user = mpstat_data.split(" ")[mpstat_header.split(" ").index('%usr')].to_f
   percent_sys = mpstat_data.split(" ")[mpstat_header.split(" ").index('%sys')].to_f
   percent_idle = mpstat_data.split(" ")[mpstat_header.split(" ").index('%idle')].to_f
   percent_io_wait = mpstat_data.split(" ")[mpstat_header.split(" ").index('%iowait')].to_f


   ###########################################################################
   server_save_data("percent_user", server_name, "GAUGE", "LAST", percent_user, "% usr", 0)
   ###########################################################################
   server_save_data("percent_sys", server_name, "GAUGE", "LAST", percent_sys, "% sys", 0)
   ###########################################################################
   server_save_data("percent_idle", server_name, "GAUGE", "LAST", percent_idle, "% idle", 0)
   ###########################################################################
   server_save_data("percent_io_wait", server_name, "GAUGE", "LAST", percent_io_wait, "% io wait", 0)
   ###########################################################################

   # number of processors:
   # mrc - save this value, only run once per server:
   #num_processors  = %x(grep processor  /proc/cpuinfo | wc -l)
   num_processors = server_get_data("galcg3", server_name, "grep processor  /proc/cpuinfo | wc -l")
   num_processors = num_processors

   # load averages:
   # cat /proc/loadavg
   #load_averages  = %x(cat /proc/loadavg)
   load_averages = server_get_data("galcg3", server_name, "cat /proc/loadavg")
   load_1 = load_averages.split(" ")[0].to_f
   load_5 = load_averages.split(" ")[1].to_f
   load_15 = load_averages.split(" ")[2].to_f

   ###########################################################################
   server_save_data("load_1", server_name, "GAUGE", "LAST", load_1, "load 1", 0)
   ###########################################################################
   server_save_data("load_5", server_name, "GAUGE", "LAST", load_5, "load 5", 0)
   ###########################################################################
   server_save_data("load_15", server_name, "GAUGE", "LAST", load_15, "load 15", 0)
   ###########################################################################


   #disk_usages  = %x(echo DISKUTIL_CMD_OP_ST;/bin/df -Pm |awk '{print $3  " " $4 " " $5  " " $6}';echo DISKUTIL_CMD_OP_END)
   disk_usages = server_get_data("galcg3", server_name, "/bin/df -Pm |awk '{print $5}'")
   max_disk_usage = disk_usages.scan(/(^\d{1,2})%$/).flatten.map{|x| x.to_i}.max

   ###########################################################################
   server_save_data("disk_usage", server_name, "GAUGE", "LAST", max_disk_usage, "max % disk usage", 0)
   ###########################################################################
end


def update_db_rrd(server_name, db_port, db_name, db_user, db_pass)



   begin
      # connect, if it has not connected before
      db_conn ||= PGconn.connect(server_name, db_port, '', '', db_name, db_user, db_pass)


      # check for connection error PGconn.connect

      #-- get the database oid for future sql statements
      db_oid = 0
      db_results = db_conn.exec(" SELECT oid FROM pg_database WHERE datname = '#{db_name}' ")

      if((PGresult::COMMAND_OK == db_results.status()) || (PGresult::TUPLES_OK == db_results.status()))
         for i in 0..db_results.num_tuples-1
            db_oid = db_results.getvalue(i, db_results.fieldnum("oid"))
         end
         puts "database oid: #{db_oid}"
      end


      #-- active connections
      sql = " select pg_stat_get_db_numbackends(#{db_oid}) "
      db_active_connections = 0
      db_results = db_conn.exec(sql)

      if((PGresult::COMMAND_OK == db_results.status()) || (PGresult::TUPLES_OK == db_results.status()))
         for i in 0..db_results.num_tuples-1
            db_active_connections = db_results.getvalue(i, 0)
         end
         puts " db_active_connections = #{db_active_connections}"
      end

      #-- transactions rolledback, transactions committed, blocks read, blocks hit
      #select xact_rollback, xact_commit, blks_read, blks_hit from pg_stat_database where datid = '16387'
      sql = " select xact_rollback, xact_commit, blks_read, blks_hit from pg_stat_database where datid = '#{db_oid}' "
      db_xact_rollback = 0
      db_xact_commit = 0
      db_blks_read = 0
      db_blks_hit = 0
      db_results = db_conn.exec(sql)

      if((PGresult::COMMAND_OK == db_results.status()) || (PGresult::TUPLES_OK == db_results.status()))
         for i in 0..db_results.num_tuples-1
            db_xact_rollback = db_results.getvalue(i, db_results.fieldnum("xact_rollback"))
            db_xact_commit = db_results.getvalue(i, db_results.fieldnum("xact_commit"))
            db_blks_read = db_results.getvalue(i, db_results.fieldnum("blks_read"))
            db_blks_hit = db_results.getvalue(i, db_results.fieldnum("blks_hit"))
         end
         puts " db_xact_rollback = #{db_xact_rollback}"
         puts " db_xact_commit = #{db_xact_commit}"
         puts " db_blks_read = #{db_blks_read}"
         puts " db_blks_hit = #{db_blks_hit}"
      end


#      # -- number of index scans
#      # -- number of index entries returned by index scans
#      # -- number of live table rows fetched by simple scans
      sql = " select sum(idx_scan),sum(idx_tup_read),sum(idx_tup_fetch) from pg_stat_all_indexes "
      db_idx_scan_indexes = 0 # returns same value as other query
      db_idx_tup_read_indexes = 0
      db_idx_tup_fetch_indexes = 0
      db_results = db_conn.exec(sql)

      if((PGresult::COMMAND_OK == db_results.status()) || (PGresult::TUPLES_OK == db_results.status()))
         for i in 0..db_results.num_tuples-1
            db_idx_scan_indexes = db_results.getvalue(i, 0)
            db_idx_tup_read_indexes = db_results.getvalue(i, 1)
            db_idx_tup_fetch_indexes = db_results.getvalue(i, 2)
         end
         puts " db_idx_scan_indexes = #{db_idx_scan_indexes}"
         puts " db_idx_tup_read_indexes = #{db_idx_tup_read_indexes}"
         puts " db_idx_tup_fetch_indexes = #{db_idx_tup_fetch_indexes}"
      end


      # -- locks granted
      #select count(*) from pg_locks where granted = 't'
      # -- locks waiting
      #select count(*) from pg_locks where granted != 't'
      # doing both lock types in one statement so they are sampling at the same time
      sql = " SELECT (SELECT COUNT(*) FROM pg_locks WHERE granted = 't' ) AS locks_granted, (SELECT COUNT(*) FROM pg_locks WHERE granted != 't') AS locks_waiting "
      db_locks_granted = 0
      db_locks_waiting = 0
      db_results = db_conn.exec(sql)

      if((PGresult::COMMAND_OK == db_results.status()) || (PGresult::TUPLES_OK == db_results.status()))
         for i in 0..db_results.num_tuples-1
            db_locks_granted = db_results.getvalue(i, db_results.fieldnum("locks_granted"))
            db_locks_waiting = db_results.getvalue(i, db_results.fieldnum("locks_waiting"))
         end
         puts " db_locks_granted = #{db_locks_granted}"
         puts " db_locks_waiting = #{db_locks_waiting}"
      end

      # -- biggest table (biggest relation)
      sql = " SELECT pg_total_relation_size(C.oid) AS biggest_table_size FROM pg_class C ORDER BY pg_total_relation_size(C.oid) DESC LIMIT 1 "
      db_biggest_table_size = 0
      db_results = db_conn.exec(sql)

      if((PGresult::COMMAND_OK == db_results.status()) || (PGresult::TUPLES_OK == db_results.status()))
         for i in 0..db_results.num_tuples-1
            db_biggest_table_size = db_results.getvalue(i, db_results.fieldnum("biggest_table_size"))
         end
         puts " db_biggest_table_size = #{db_biggest_table_size}"
      end

      # -- disk use: table size
      # -- disk use: index size
      sql = "
      SELECT ( SELECT sum(pg_relation_size(C.oid))
                 FROM pg_class C
                 LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
                 WHERE nspname NOT IN ('pg_catalog', 'information_schema')
                 AND relname NOT like '%pkey' ) AS total_table_size,
             ( SELECT sum(pg_relation_size(C.oid))
                 FROM pg_class C
                 LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
                 WHERE nspname NOT IN ('pg_catalog', 'information_schema')
                     AND relname like '%pkey') AS total_index_size
      "
      db_total_table_size = 0
      db_total_index_size = 0
      db_results = db_conn.exec(sql)

      if((PGresult::COMMAND_OK == db_results.status()) || (PGresult::TUPLES_OK == db_results.status()))
         for i in 0..db_results.num_tuples-1
            db_total_table_size = db_results.getvalue(i, db_results.fieldnum("total_table_size"))
            db_total_index_size = db_results.getvalue(i, db_results.fieldnum("total_index_size"))
         end
         puts " db_total_table_size = #{db_total_table_size}"
         puts " db_total_index_size = #{db_total_index_size}"
      end

      # -- query stats, total insert, update, and delete, table level scan seq scan, table level index scan, sequential scan ROWS, table index scan ROWS

      sql = " SELECT SUM(n_tup_ins) AS n_tup_ins, SUM(n_tup_upd) AS n_tup_upd, SUM(n_tup_del) AS n_tup_del, SUM(seq_scan) AS seq_scan, SUM(idx_scan) as idx_scan, SUM(seq_tup_read) AS seq_tup_read ,SUM(idx_tup_fetch) AS idx_tup_fetch FROM pg_stat_all_tables "
      db_n_tup_ins = 0
      db_n_tup_upd = 0
      db_n_tup_del = 0
      db_seq_scan = 0
      db_idx_scan = 0
      db_seq_tup_read = 0
      db_idx_tup_fetch = 0
      db_results = db_conn.exec(sql)

      if((PGresult::COMMAND_OK == db_results.status()) || (PGresult::TUPLES_OK == db_results.status()))
         for i in 0..db_results.num_tuples-1
            db_n_tup_ins = db_results.getvalue(i, db_results.fieldnum("n_tup_ins"))
            db_n_tup_upd = db_results.getvalue(i, db_results.fieldnum("n_tup_upd"))
            db_n_tup_del = db_results.getvalue(i, db_results.fieldnum("n_tup_del"))
            db_seq_scan = db_results.getvalue(i, db_results.fieldnum("seq_scan"))
            db_idx_scan = db_results.getvalue(i, db_results.fieldnum("idx_scan"))
            db_seq_tup_read = db_results.getvalue(i, db_results.fieldnum("seq_tup_read"))
            db_idx_tup_fetch = db_results.getvalue(i, db_results.fieldnum("idx_tup_fetch"))
         end
         puts " db_n_tup_ins = #{db_n_tup_ins}"
         puts " db_n_tup_upd = #{db_n_tup_upd}"
         puts " db_n_tup_del = #{db_n_tup_del}"
         puts " db_seq_scan = #{db_seq_scan}"
         puts " db_idx_scan = #{db_idx_scan}"
         puts " db_seq_tup_read = #{db_seq_tup_read}"
         puts " db_idx_tup_fetch = #{db_idx_tup_fetch}"
      end

      ###########################################################################
      # save the data and generate the graphs
      ###########################################################################
      server_save_data("db_active_connections", server_name, "GAUGE", "LAST", db_active_connections, "connections", 0)
      server_save_data("db_xact_rollback", server_name, "DERIVE", "LAST", db_xact_rollback, "trans. rolledback", 0)
      server_save_data("db_xact_commit", server_name, "DERIVE", "LAST", db_xact_commit, "trans. committed", 0)
      server_save_data("db_blks_read", server_name, "DERIVE", "LAST", db_blks_read, "blocks read", 0)
      server_save_data("db_blks_hit", server_name, "DERIVE", "LAST", db_blks_read, "blocks hit", 0)
      #server_save_data("db_idx_scan_indexes", server_name, "DERIVE", "LAST", db_idx_scan_indexes, "index scans", 0)
      server_save_data("db_idx_tup_read_indexes", server_name, "DERIVE", "LAST", db_idx_tup_read_indexes, "index entries from scans", 0)
      server_save_data("db_idx_tup_fetch_indexes", server_name, "DERIVE", "LAST", db_idx_tup_fetch_indexes, "rows fetched by scans", 0)
      server_save_data("db_locks_granted", server_name, "GAUGE", "LAST", db_locks_granted, "locks granted", 0)
      server_save_data("db_locks_waiting", server_name, "GAUGE", "LAST", db_locks_waiting, "locks waiting", 0)
      server_save_data("db_biggest_table_size", server_name, "GAUGE", "LAST", db_biggest_table_size, "biggest table size", 9)
      server_save_data("db_total_table_size", server_name, "GAUGE", "LAST", db_total_table_size, "total table size", 9)
      server_save_data("db_total_index_size", server_name, "GAUGE", "LAST", db_total_index_size, "total index size", 9)
      server_save_data("db_n_tup_ins", server_name, "DERIVE", "LAST", db_n_tup_ins, "total insert", 0)
      server_save_data("db_n_tup_upd", server_name, "DERIVE", "LAST", db_n_tup_upd, "total update", 0)
      server_save_data("db_n_tup_del", server_name, "DERIVE", "LAST", db_n_tup_del, "total delete", 0)
      server_save_data("db_seq_scan", server_name, "DERIVE", "LAST", db_seq_scan, "table scan", 0)
      server_save_data("db_idx_scan", server_name, "DERIVE", "LAST", db_idx_scan, "index scan", 0)
      server_save_data("db_seq_tup_read", server_name, "DERIVE", "LAST", db_seq_tup_read, "scan table ROWS", 0)
      server_save_data("db_idx_tup_fetch", server_name, "DERIVE", "LAST", db_idx_tup_fetch, "scan index ROWS", 0)

   #rescue PG::Error => err
   rescue PGError => err
      # mrc - test this
      p [
         err.result.error_field( PG::Result::PG_DIAG_SEVERITY ),
         err.result.error_field( PG::Result::PG_DIAG_SQLSTATE ),
         err.result.error_field( PG::Result::PG_DIAG_MESSAGE_PRIMARY ),
         err.result.error_field( PG::Result::PG_DIAG_MESSAGE_DETAIL ),
         err.result.error_field( PG::Result::PG_DIAG_MESSAGE_HINT ),
         err.result.error_field( PG::Result::PG_DIAG_STATEMENT_POSITION ),
         err.result.error_field( PG::Result::PG_DIAG_INTERNAL_POSITION ),
         err.result.error_field( PG::Result::PG_DIAG_INTERNAL_QUERY ),
         err.result.error_field( PG::Result::PG_DIAG_CONTEXT ),
         err.result.error_field( PG::Result::PG_DIAG_SOURCE_FILE ),
         err.result.error_field( PG::Result::PG_DIAG_SOURCE_LINE ),
         err.result.error_field( PG::Result::PG_DIAG_SOURCE_FUNCTION ),
      ]
   end




end

def  generate_png(rrd_file, rrd_image, rrd_title, label, consolidate_function, units_exponent)

   # getting the times from the file - to allow for restarting the program and keeping old data
   # don't pass in the start and end times, look them up
   (fstart, fend, data, step) = RRD.fetch(rrd_file, consolidate_function) # "AVERAGE"
   puts "fetch: data points:#{data.length}, from #{fstart} to #{fend}. step length:#{step.length}\n"

   puts "generating graph #{rrd_image}"

   # use either the file start time of the last 4 hours (which ever is smaller)
   fstart = [fstart, Time.now.to_i - (60 * 60 * 4)].max
   RRD.graph(
      "#{rrd_image}",
       "--title", " #{rrd_title} ",
#       "--watermark", "canann",
       "--vertical-label", " #{label}",
#       "--x-grid", "SECOND:10:MINUTE:1:MINUTE:2:0:%X",
       "--start", "#{fstart}",
#       "--end", "#{Time.now.to_i}",
       "--interlace",
       "--imgformat", "PNG",
       "--width=900",
       "--slope-mode",
       "--units-exponent", "#{units_exponent}",
       "--units-length", "8", # to make room for the large numbers (disk used)
       "DEF:ds0=#{rrd_file}:ds0:#{consolidate_function}", #AVERAGE
       "CDEF:load=ds0,1,*",
       "LINE1:load#0011FF",
       "AREA:ds0#0011FF:Acutal\\n",
       "VDEF:max=ds0,MAXIMUM", # calculated maximum for the whole period
       "LINE1:max#FF0000:Maximum\\t",
       "GPRINT:max:%2.3lf\\n",
       "VDEF:min=ds0,MINIMUM", # calculated minimum for the whole period
       "LINE1:min#00FF00:Minimum\\t",
       "GPRINT:min:%2.3lf\\n",
       "VDEF:average=ds0,AVERAGE", # calculated minimum for the whole period
       "LINE1:average#555555:Average\\t",
       "GPRINT:average:%2.3lf\\n",
       "VDEF:last=ds0,LAST", # lastest measurment
       "COMMENT:  Last\\t\\t",
       "GPRINT:last:%2.3lf\\n"
       )
   puts

end



while(true)


   # list of servers to check
   # # mrc pull this in from a config file
   servers = %w(server1 server2 server3)

   db_servers = %w(dbserver)
   db_ports = %w(5432)
   db_names = %w(REALDB)
   db_users = %w(user)
   db_passs = %w(pass)


   threads = []

   # server things to check:
   #    user time %
   #    system time %
   #    idle time %
   #    I/O wait time %
   #
   #    swap memory utilization
   #    physical mdmory utilization
   #
   #    load: jobs in 1 minute - mrc - divide by processors?
   #    load: jobs in 5 minutes
   #    load: jobs in 15 minutes
   #
   #    disk usage

   run_time = Time.now.to_i

   # mrc - make each server check multi threaded
   # once threads are being used, increase the check interval to once a minute
   servers.each { |server|

      threads << Thread.new{

         Thread.current["name"] = server

         puts "***********************************************\n server:#{server}."

         # create if necessary, reads the data and calls the functions to generate the image
         update_server_rrd(server)
      }
   }

   # mrc - review this against the sql statements
   # postgres things to check:
   #    active connections
   #    buffer stats: buffer hits/min
   #    buffer stats: buffer reads/min
   #    disk usage details: disk usage
   #    disk usage details: index usage
   #    index scan details: index scans/min
   #    index scan details: index reads/min
   #    index scan details: index fetches/min
   #    lock stats: locks held
   #    lock stats: locks wait
   #    size of largest table
   #    query statistics: row inserts/min
   #    query statistics: row updates/min
   #    query statistics: row deletes/min
   #    table level scan details: sequential scans/min
   #    table level scan details: table index scans/min
   #    table level scan details: sequential scan rows read/min
   #    table level scan details: table index scan rows read/min
   #    transaction details: commits/min
   #    transaction details: rollbacks/min
   db_servers.each_with_index { |db_server, i|


      threads << Thread.new{

         Thread.current["name"] = db_server

         puts "*********************************************** db server:#{db_servers[i]} #{db_ports[i]} #{db_names[i]} #{db_users[i]} - #{db_passs[i]}."



         # create if necessary, reads the data and calls the functions to generate the image
         update_db_rrd(db_servers[i], db_ports[i], db_names[i], db_users[i], db_passs[i])
      }

   }

   threads.each do |th|
      th.join
      puts "#{th.inspect}: #{th[:name]}"
   end

   puts "***********************************************"
   puts "\nCount down to next run:"
   chars = %w{ | / - \\ }
   while(Time.now.to_i < (run_time + 58))
      print "\r#{chars[0]} - #{(run_time + 58) - Time.now.to_i}   \r"
      sleep 0.1
      chars.push chars.shift
   end

end

